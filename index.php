<?php
require_once ("animal.php");
require_once ("frog.php");
require_once ("ape.php");



$sheep = new Animal("shaun");

echo "<h3>Release 0</h3>";
echo "Name : ". $sheep->name; // "shaun"
echo "<br>";
echo $sheep->get_legs();//4
//echo $sheep->legs; // 4
echo "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded; // "no"
echo "<br>";
echo "<h3>Release 1</h3>";

$ampibi = new Frog("buduk");
echo "Name : ". $ampibi->name; // "buduk"
echo "<br>";
echo $ampibi->get_legs(); // 4
echo "<br>";
echo "Cold Blooded : " . $ampibi->cold_blooded; // "no"
echo "<br>";
echo $ampibi->jump();//Hop Hop
//echo $sheep->legs; // 4
echo "<br>";
echo "<br>";
$mamalia = new Ape("Kera Sakti");
echo "Name : ". $mamalia->name; // "kera sakti"
echo "<br>";
echo $mamalia->get_legs(); // 2
echo "<br>";
echo "Cold Blooded : " . $mamalia->cold_blooded; // "no"
echo "<br>";
echo $mamalia->yell();//Auouo
echo "<br>";



?>