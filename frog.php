<?php

class Frog extends Animal
{
    public $legs = "4";
    public $cold_blooded = "No";
    public $name;
    public function __construct($name){
        $this->name = $name;
    }
    public function get_legs(){
        echo "Legs : 4";
    }
    public function jump(){
        echo "Jump : Hop Hop";
    }
}



?>